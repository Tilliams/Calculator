import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

//Testwndow class inherits from JFrame (methods and/or variables)
public class CalculatorAdvanced extends JFrame
{
	
/*access DataType variableName(s)
Creating labels, panel, buttons, and text field variables*/
private JLabel label1, label2;
private JPanel panel;
private JTextField text1, text2;
private JButton buttonM, buttonA, buttonD, buttonS;
private JButton buttonSin, buttonCos, buttonTan, buttonLog, buttonNatLog, buttonE, buttonRt;

//setting width and height dimensions
private final int WIDTH=300, HEIGHT=250;

//Constructor uses some methods from "JFrame" Class
	public CalculatorAdvanced()
	{

	//Title and size of window
	setTitle("Calculator");
	setSize(WIDTH, HEIGHT);
	
	//Here We Construct the Panel
	buildPanel();

	//Add method. Adds the panel to the window 
	add(panel);
	
	
	setVisible(true);

	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}


	public void buildPanel()
	{

	//Iniitialize label variables
	label1 = new JLabel("Enter first number");
	label2 = new JLabel("Enter second number");

	//intitializing text fields of 10 columns wide
	text1 = new JTextField(10);
	text2 = new JTextField(10);

	//first line creates button and the next line
	buttonM = new JButton("x");
	buttonM.addActionListener(new mulListener());

	buttonD = new JButton("/");
	buttonD.addActionListener(new divListener());

	buttonA = new JButton("+");
	buttonA.addActionListener(new addListener());

	buttonS = new JButton("-");
	buttonS.addActionListener(new subListener());

	buttonSin = new JButton("sin");
	buttonSin.addActionListener(new sinListener());

	buttonCos = new JButton("cos");
	buttonCos.addActionListener(new cosListener());

	buttonTan = new JButton("tan");
	buttonTan.addActionListener(new tanListener());

	buttonLog = new JButton("log");
	buttonLog.addActionListener(new logListener());

	buttonNatLog = new JButton("ln");
	buttonNatLog.addActionListener(new natListener());

	buttonRt = new JButton("SqRt");
	buttonRt.addActionListener(new rtListener());

	buttonE = new JButton("e");
	buttonE.addActionListener(new eListener());

	//Initializes Panel Variable
	panel = new JPanel();

	//Adds everything to the panel
	panel.add(label1);
	panel.add(text1);
	panel.add(label2);
	panel.add(text2);
	panel.add(buttonM);
	panel.add(buttonS);
	panel.add(buttonD);
	panel.add(buttonA);
	panel.add(buttonSin);
	panel.add(buttonCos);
	panel.add(buttonTan);
	panel.add(buttonLog);
	panel.add(buttonNatLog);
	panel.add(buttonE);	
	panel.add(buttonRt);
	}


	private class mulListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputM1, inputM2;
		double valM1, valM2, prod;

		inputM1=text1.getText();
		valM1=Double.parseDouble(inputM1);

		inputM2=text2.getText();
		valM2=Double.parseDouble(inputM2);

		prod=valM1*valM2;

		JOptionPane.showMessageDialog(null, inputM1+ " x "+inputM2+" = "+prod);
		}
	}


	//Handles Division Button
	private class divListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputD1, inputD2;
		double valD1, valD2, quot;

		inputD1=text1.getText();
		valD1=Double.parseDouble(inputD1);

		inputD2=text2.getText();
		valD2=Double.parseDouble(inputD2);

		quot=valD1/valD2;

		JOptionPane.showMessageDialog(null, inputD1+ "/"+inputD2+" = "+quot);
		}
	}

	private class subListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputS1, inputS2;
		double valS1, valS2, diff;

		inputS1=text1.getText();
		valS1=Double.parseDouble(inputS1);

		inputS2=text2.getText();
		valS2=Double.parseDouble(inputS2);

		diff=valS1-valS2;

		JOptionPane.showMessageDialog(null, inputS1+ " - "+inputS2+" = "+diff);
		}
	}

	private class addListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputA1, inputA2;
		double valA1, valA2, sum;

		inputA1=text1.getText();
		valA1=Double.parseDouble(inputA1);

		inputA2=text2.getText();
		valA2=Double.parseDouble(inputA2);

		sum=valA1+valA2;

		JOptionPane.showMessageDialog(null, inputA1+" + "+inputA2+" = "+sum);
		}
	}

	private class sinListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputSin1, inputSin2;
		double valSin1, valSin2;

		inputSin1=text1.getText();
		valSin1=Double.parseDouble(inputSin1);

		inputSin2=text2.getText();
		valSin2=Double.parseDouble(inputSin2);

		valSin1=(valSin1*Math.PI)/180;
		valSin2=(valSin2*Math.PI)/180;

		valSin1=Math.sin(valSin1);
		valSin2=Math.sin(valSin2);

		JOptionPane.showMessageDialog(null, "sin("+inputSin1+") = "+valSin1+"\nsin("+inputSin2+") = "+valSin2);
		}
	}

	private class cosListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputCos1, inputCos2;
		double valCos1, valCos2;

		inputCos1=text1.getText();
		valCos1=Double.parseDouble(inputCos1);

		inputCos2=text2.getText();
		valCos2=Double.parseDouble(inputCos2);

		valCos1=(valCos1*Math.PI)/180;
		valCos2=(valCos2*Math.PI)/180;

		valCos1=Math.cos(valCos1);
		valCos2=Math.cos(valCos2);

		JOptionPane.showMessageDialog(null, "cos("+inputCos1+") = "+valCos1+"\ncos("+inputCos2+") = "+valCos2);
		}
	}

	private class tanListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputTan1, inputTan2;
		double valTan1, valTan2;

		inputTan1=text1.getText();
		valTan1=Double.parseDouble(inputTan1);

		inputTan2=text2.getText();
		valTan2=Double.parseDouble(inputTan2);

		valTan1=(valTan1*Math.PI)/180;
		valTan2=(valTan2*Math.PI)/180;

		valTan1=Math.tan(valTan1);
		valTan2=Math.tan(valTan2);

		JOptionPane.showMessageDialog(null, "tan("+inputTan1+") = "+valTan1+"\ntan("+inputTan2+") = "+valTan2);
		}
	}

	private class logListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputLog1, inputLog2;
		double valLog1, valLog2;

		inputLog1=text1.getText();
		valLog1=Double.parseDouble(inputLog1);
		
		inputLog2=text2.getText();
		valLog2=Double.parseDouble(inputLog2);

		valLog1=Math.log10(valLog1);
		valLog2=Math.log10(valLog2);

		JOptionPane.showMessageDialog(null, "log("+inputLog1+") = "+valLog1+"\nlog("+inputLog2+") = "+valLog2);
		}
	}

	private class natListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputNatLog1, inputNatLog2;
		double valNatLog1, valNatLog2;

		inputNatLog1=text1.getText();
		valNatLog1=Double.parseDouble(inputNatLog1);

		inputNatLog2=text2.getText();
		valNatLog2=Double.parseDouble(inputNatLog2);

		valNatLog1=Math.log(valNatLog1);
		valNatLog2=Math.log(valNatLog2);

		JOptionPane.showMessageDialog(null, "ln("+inputNatLog1+") = "+valNatLog1+"\nln("+inputNatLog2+") = "+valNatLog2);
		}
	}

	private class eListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputE1, inputE2;
		double valE1, valE2;

		inputE1=text1.getText();
		valE1=Double.parseDouble(inputE1);

		inputE2=text2.getText();
		valE2=Double.parseDouble(inputE2);

		valE1=Math.exp(valE1);
		valE2=Math.exp(valE2);

		JOptionPane.showMessageDialog(null, "e^"+inputE1+" = "+valE1+"\ne^"+inputE2+" = "+valE2);
		}
	}

	private class rtListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

		String inputRT1, inputRT2;
		double valRT1, valRT2;

		inputRT1=text1.getText();
		valRT1=Double.parseDouble(inputRT1);

		inputRT2=text2.getText();
		valRT2=Double.parseDouble(inputRT2);

		valRT1=Math.sqrt(valRT1);
		valRT2=Math.sqrt(valRT2);

		JOptionPane.showMessageDialog(null, "Square root of "+inputRT1+" = "+valRT1+"\nSquare root of "+inputRT2+" = "+valRT2);
		}
	}
	
	public static void main(String[] args)
	{
		new CalculatorAdvanced();
	}
}

